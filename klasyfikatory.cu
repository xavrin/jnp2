#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#define SAFE_NO_SYNC(call) do { \
    cudaError err = call; \
    if (cudaSuccess != err) { \
        fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n", \
                __FILE__, __LINE__, cudaGetErrorString(err)); \
        exit(EXIT_FAILURE); \
    }} while (0);

#define SAFE(call) do { \
    SAFE_NO_SYNC(call); \
    cudaError err = cudaThreadSynchronize(); \
    if (cudaSuccess != err) { \
        fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n", \
                __FILE__, __LINE__, cudaGetErrorString(err)); \
        exit(EXIT_FAILURE); \
    }} while (0);

#define T 3
#define get(array, i, j, ROW) (array[i*ROW+j])
#define BLOCK_DIM 16 /* our block will be a square with such side's length */
#define OBJECTS BLOCK_DIM /* amount of objects held in shared memory */

void cuda_error(cudaError_t code) {
    fprintf(stderr, "CUDA ERROR: %s\n", cudaGetErrorString(code));
    exit(1);
}

void error_exit(const char *msg) {
    fprintf(stderr, "Error: %s\n", msg);
    exit(1);
}

/* structure for the results */
typedef struct res_t {
    float r;
    int i, j;
    short x, y;
} res_t;

/* objectsi - bitmasks for the values of 'genes' for the objects; bit 1 on
 *            position (k*W + i) in objectsj means that object i has value j
 *            of gene k
 * W - objects count
 * H - variables count
 * one_prob - probability of 'one' value
 * S - cut-off factor
 * results - table for the results
 * count - count of the entries in results' table
 * row - current row of blocks we're calculating
 * border - the first number of object with value 1 of 'healthy-sick'
 *          variable */
__global__ void classifiers(long long *objects0, long long *objects1,
        long long *objects2, int W, int H, float one_prob, float S,
        res_t *results, unsigned *count, int row, int border) {
    /* i, j - a pair of variables that this thread will count */
    int i = row * blockDim.y + threadIdx.y,
        j = (row + blockIdx.x) * blockDim.x + threadIdx.x,
        x, y, a, tmp; /* variables for iteration or temporary use */
    /* it's an ugly piece of code, but it's the only way to place sum values
     * in registers
     * SiVj[k] - i stands for the 'sick-healthy' variable
     *           j - stands for the value of the first gene
     *           k - stands for the value of the second gene */
    int S0V0[3], S0V1[3], S0V2[3], S1V0[3], S1V1[3], S1V2[3];
    float E, diff; /* expected value and difference between E and standard
                      deviation */
    /* every block is responsible for the square [a,b]x[c,d] of genes; I
     * will hold 3 bitmasks with values of objects for genes [a,b]; J for
     * genes [c,d] */
    __shared__ long long I[BLOCK_DIM][3][OBJECTS+1];
    __shared__ long long J[BLOCK_DIM][3][OBJECTS+1];
    S0V0[0] = S0V0[1] = S0V0[2] = 0;
    S0V1[0] = S0V1[1] = S0V1[2] = 0;
    S0V2[0] = S0V2[1] = S0V2[2] = 0;
    S1V0[0] = S1V0[1] = S1V0[2] = 0;
    S1V1[0] = S1V1[1] = S1V1[2] = 0;
    S1V2[0] = S1V2[1] = S1V2[2] = 0;
    for (x = 0; x < W; x += OBJECTS) {
        tmp = min(OBJECTS, W - x); /* amount of objects we want to obtain this
                                      time */
        for (y = 0; y + threadIdx.x < tmp; y += blockDim.x) {
            I[threadIdx.y][0][y + threadIdx.x] =
                get(objects0, i, x + y + threadIdx.x, W);
            I[threadIdx.y][1][y + threadIdx.x] =
                get(objects1, i, x + y + threadIdx.x, W);
            I[threadIdx.y][2][y + threadIdx.x] =
                get(objects2, i, x + y + threadIdx.x, W);
        }
        for (y = 0; y + threadIdx.y < tmp; y += blockDim.y) {
            J[threadIdx.x][0][y + threadIdx.y] =
                get(objects0, j, x + y + threadIdx.y, W);
            J[threadIdx.x][1][y + threadIdx.y] =
                get(objects1, j, x + y + threadIdx.y, W);
            J[threadIdx.x][2][y + threadIdx.y] =
                get(objects2, j, x + y + threadIdx.y, W);
        }
        __syncthreads();
        /* since we have masks for every possible value of genes, we can simply
         * use AND for obtaining count of objects for all combinations */
        for (y = 0; y < tmp; y++) {
            if (x + y >= border) {
                for (a = 0; a < 3; a++)
                    S1V0[a] += __popcll(
                        I[threadIdx.y][0][y] & J[threadIdx.x][a][y]);
                for (a = 0; a < 3; a++)
                    S1V1[a] += __popcll(
                        I[threadIdx.y][1][y] & J[threadIdx.x][a][y]);
                for (a = 0; a < 3; a++)
                    S1V2[a] += __popcll(
                        I[threadIdx.y][2][y] & J[threadIdx.x][a][y]);
            }
            else {
                for (a = 0; a < 3; a++)
                    S0V0[a] += __popcll(
                        I[threadIdx.y][0][y] & J[threadIdx.x][a][y]);
                for (a = 0; a < 3; a++)
                    S0V1[a] += __popcll(
                        I[threadIdx.y][1][y] & J[threadIdx.x][a][y]);
                for (a = 0; a < 3; a++)
                    S0V2[a] += __popcll(
                        I[threadIdx.y][2][y] & J[threadIdx.x][a][y]);
            }
        }
        __syncthreads();
    }
    for (a = 0; a < 3; a++) {
        E = one_prob * (S0V0[a] + S1V0[a] + 1);
        diff = S1V0[a] + one_prob - E;
        if (diff * diff > S * S * E && i < j)
            results[atomicAdd(count, 1)] =
                (res_t){.r = fabs(diff)/sqrt(E), .i=i, .j=j, .x=0, .y=a};
    }
    for (a = 0; a < 3; a++) {
        E = one_prob * (S0V1[a] + S1V1[a] + 1);
        diff = S1V1[a] + one_prob - E;
        if (diff * diff > S * S * E && i < j)
            results[atomicAdd(count, 1)] =
                (res_t){.r = fabs(diff)/sqrt(E), .i=i, .j=j, .x=1, .y=a};
    }
    for (a = 0; a < 3; a++) {
        E = one_prob * (S0V2[a] + S1V2[a] + 1);
        diff = S1V2[a] + one_prob - E;
        if (diff * diff > S * S * E && i < j)
            results[atomicAdd(count, 1)] =
                (res_t){.r = fabs(diff)/sqrt(E), .i=i, .j=j, .x=2, .y=a};
    }
}

int main(int argc, char* argv[]) {
    int K, N, W, H;
    float S;
    int i, j, l, r, tmp, s;
    long long *objects[T], *dev_objects[T];
    int *one_object;
    res_t *dev_results, *results;
    unsigned count = 0, *dev_count;
    int one_count = 0;
    float one_prob;
    struct timeval st, en;
    int OBJ_PER_VAR = (sizeof *objects) * 8;
    if (argc < 2)
        error_exit("Too few arguments");
    S = atof(argv[1]);
    scanf("%d %d", &K, &N);
    H = K;
    if (H % BLOCK_DIM)
        H += BLOCK_DIM - K % BLOCK_DIM;
    /* we want all objects variables to contain either all zeros or all ones */
    W = N / OBJ_PER_VAR + 1;
    if (N % OBJ_PER_VAR)
        W++;
    /* l will stand for the position of the next 0-object, r - 1-object */
    l = 0; r = W * OBJ_PER_VAR - 1;
    for (i = 0; i < T; i++) {
        if (!(objects[i] = (long long*)malloc(W * H * sizeof *objects[i])))
            error_exit("malloc");
        memset(objects[i], 0LL, W * H * sizeof *objects[i]);
    }
    if (!(one_object = (int*)malloc(K * sizeof *one_object)))
        error_exit("malloc");
    if (!(results = (res_t*)malloc(T*T*BLOCK_DIM * K * sizeof *results)))
        error_exit("malloc");
    for (i = 0; i < N; i++) {
        /* since we don't know whether it is a zero or one, we have to read
         * that to another memory */
        for (j = 0; j < K; j++)
            scanf("%d", &one_object[j]);
        scanf("%d", &tmp);
        if (tmp) {
            s = r--;
            one_count++;
        }
        else
            s = l++;
        /* we just ignore N/A values */
        for (j = 0; j < K; j++) if (one_object[j] != 3)
            get(objects[one_object[j]], j, s / OBJ_PER_VAR, W)
                |= (1LL << (s % OBJ_PER_VAR));
    }
    gettimeofday(&st, NULL);
    for (i = 0; i < T; i++)
        SAFE_NO_SYNC(cudaMalloc(&dev_objects[i],
                                W * H * sizeof *dev_objects[i]))
    SAFE_NO_SYNC(cudaMalloc(&dev_results,
                            T*T*BLOCK_DIM * K * sizeof *dev_results));
    SAFE_NO_SYNC(cudaMalloc(&dev_count, sizeof *dev_count));
    SAFE_NO_SYNC(cudaDeviceSetCacheConfig(cudaFuncCachePreferShared));
    for (i = 0; i < T; i++)
        SAFE_NO_SYNC(cudaMemcpy(dev_objects[i], objects[i],
            W * H * sizeof *objects, cudaMemcpyHostToDevice))
    /* counting the probability */
    one_prob = (float)one_count / N;
    for (i = 0; i < H / BLOCK_DIM; i++) {
        dim3 threadsPerBlock(BLOCK_DIM, BLOCK_DIM);
        dim3 numBlocks(H / BLOCK_DIM - i, 1);
        count = 0;
        SAFE_NO_SYNC(cudaMemcpy(dev_count, &count, sizeof count,
                                cudaMemcpyHostToDevice));
        classifiers<<<numBlocks, threadsPerBlock>>>(
            dev_objects[0], dev_objects[1], dev_objects[2], W, H,
            one_prob, S, dev_results, dev_count, i, (r+1) / OBJ_PER_VAR);
        SAFE(cudaMemcpy(&count, dev_count, sizeof count, cudaMemcpyDeviceToHost));
        SAFE(cudaMemcpy(results, dev_results, count * sizeof *results,
            cudaMemcpyDeviceToHost));
        for (j = 0; j < count; j++)
            printf("%d %d %d %d %f\n", results[j].j, results[j].i,
                results[j].y, results[j].x, results[j].r);
    }
    gettimeofday(&en, NULL);
    long long usecs = (en.tv_sec - st.tv_sec) * 1000000LL +
        en.tv_usec - st.tv_usec;
    fprintf(stderr, "execution time: %llds, %lldms\n", usecs / 1000000,
        (usecs % 1000000) / 1000);
    for (i = 0; i < T; i++) {
        free(objects[i]);
        cudaFree(dev_objects[i]);
    }
    free(results);
    free(one_object);
    cudaFree(dev_count);
    cudaFree(dev_results);
    cudaDeviceReset();
    return 0;
}
