#!/bin/bash
make
SIGMAS=(0 3 1 2 0 8)
for ((i=1; i <= 5; ++i))
do
    ./klasyfikatory ${SIGMAS[i]} < testy_popr/popr$i.in > tmp
    sort tmp > sorted1
    sort testy_popr/popr$i.out > sorted2
    if diff sorted1 sorted2 > /dev/null
    then
        echo "TEST $i PASSED"
    else
        echo "TEST $i FAILED"
    fi
    rm tmp sorted1 sorted2
done
