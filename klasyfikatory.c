#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define T 3

#define get(array, i, j, ROW) (array[i*ROW+j])

void error_exit(const char *msg) {
    fprintf(stderr, "Error: %s\n", msg);
    exit(1);
}

int main(int argc, char* argv[]) {
    int K, N;
    float S;
    int i, j, x, y, ii, jj, n;
    int *objects, *results;
    int one_count = 0;
    float one_prob, sigma;
    int Sum[2][T][T];
    if (argc < 2) {
        fprintf(stderr, "Usage %s sigma_level\n", argv[0]);
        exit(1);
    }
    S = atof(argv[1]);
    scanf("%d %d", &K, &N);
    if (!(objects = malloc(N * K * sizeof *objects)))
        error_exit("malloc");
    if (!(results = malloc(N * sizeof *results)))
        error_exit("malloc");
    for (i = 0; i < N; i++) {
        for (j = 0; j < K; j++)
            scanf("%d", &get(objects, i, j, K));
        scanf("%d", &results[i]);
    }
    for (i = 0; i < N; i++)
        one_count += results[i];
    one_prob = (float)one_count / N;
    for (i = 0; i < K; i++) for (j = 0; j < i; j++) {
        for (x = 0; x < T; x++)
            for (y = 0; y < T; y++)
                Sum[0][x][y] =  Sum[1][x][y] = 0;
        for (n = 0; n < N; n++) {
            ii = get(objects, n, i, K);
            jj = get(objects, n, j, K);
            Sum[results[n]][ii][jj] += 1;
        }
        for (x = 0; x < T; x++) for (y = 0; y < T; y++) {
            float E = one_prob * (Sum[0][x][y] + Sum[1][x][y] + 1),
                  diff = fabs(Sum[1][x][y] + one_prob - E);
            sigma = sqrt(E);
            if (diff > S * sigma)
                printf("%d %d %d %d %f\n", i, j, x, y, diff / sigma);
        }
    }
    free(objects);
    free(results);
    return 0;
}
