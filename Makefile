all: klasyfikatory

%: %.cu
	nvcc -arch=sm_20 -Xptxas='-v' -Xcompiler='-Wno-cpp' $< -o $@

clean:
	rm $(all)
