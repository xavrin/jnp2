/* 
Generator danych testowych do zadania nr 2

Parametry wejsciowe:
K - liczba zmiennych
N - liczba obiektow
seed - seed dla generatora liczb losowych

Wyjscie:
Program wypisuje na standardowe wyjscie:
w pierwszej linii:
K N
dalej N linii (po 1 dla kazdego obiektu)
w kazdej linii K liczb {0,1,2} (wartosci zmiennych) oraz zmienna decyzyjna {0,1}

Przyklad wejscia:
4 5 0

Przyklad wyjscia:
4 5
0 1 0 1 0
1 0 2 1 0
1 0 1 1 0
2 1 1 1 0
2 1 1 2 0

Ogolny opis:
Z zalozenia ten kod powinien generowac dane dla ktorych najwieksza istotnosc
ma para zmiennych (0,1) o wartosciach (2,2).


-----------------------------------------------------
Przykladowe wyniki z programu z wersji nr 2 zadania:

Dane wygenerowane nastepujaca komenda:
./generateTestData 100 512 0

Przetworzone z poziomem odciecia 10.0\sigma zwracaja wynik:
(numer 1 zmiennej, numer 2 zmiennej, wartosc 1 zmiennej, wartosc 2 zmiennej, istotnosc)
1 0 2 2 10.629086

-----------------------------------------------------
./generateTestData 256 512 0

Poziom odciecia: 6.1\sigma
1 0 2 2 6.364714
-----------------------------------------------------
./generateTestData 1024 512 0

Poziom odciecia: 7.0\sigma
1 0 2 2 7.522080

te same dane na poziomie odciecia 6.3\sigma:
1 0 2 2 7.522080
511 446 2 2 6.334795
775 496 2 2 6.334795
798 548 2 2 6.334795
748 652 2 2 6.334795
880 804 2 2 6.334795
1013 880 2 2 6.751045
-----------------------------------------------------
*/



#include<cstdio>
#include<ctime>
#include<cstdlib>


float randFloat(float min, float max) {
	float rand01 = rand()/(float)RAND_MAX;
	float random = rand01*(max-min) + min;
	return random;
}

int main(int argc, char *argv[]) {
	unsigned int seed = atoi(argv[3]);
	//srand(time(NULL));
	srand(seed);
	int K = atoi(argv[1]);
	int N = atoi(argv[2]);


	printf("%d %d\n", K, N);


	for(int i=0;i<N;i++) {
		int var0;
		int var1;
		for(int j=0 ;j < K ; j++) {
			float pdbZero = randFloat(0.55f, 0.80f);
			float pdbOne  = 1.0f - pdbZero;


			float probability = randFloat(0.0f, 1.0f);

			int var;

			if ( probability <= pdbZero * pdbZero ) {
				var = 0;
			}
			else {
				probability -= pdbZero * pdbZero;
				if ( probability > 0.0f && probability <= 2.0f * pdbZero *
						(1.0f-pdbZero))
					var = 1;
				else
					var = 2;
			}

			if (j==0)
				var0 = var;
			if (j==1)
				var1 = var;

			printf("%d ", var);
		}
		int decision = 0;
		if (randFloat(0.0f, 1.0f) > 0.95f)
			decision=1;

		if ( var0==2 && var1==2) {
			decision = 1;
		}

		printf("%d\n", decision);

	}

	return 0;
}
		//int decision = 0;

		//if (var0==2 && var1==2)
		//	decision = 1;
		//if (var0==2 && var1==1)
		//	decision = 1;
		//if (var0==1 && var1==2) {
		//	if ( randfloat(0.f, 1.f) > 0.5f )
		//		decision =1;
		//	else
		//		decision =0;
		//}

		//if (var0==1 && var1 ==1) {
		//	if ( randFloat(0.f, 1.f) > 0.75f )
		//		decision =1;
		//	else
		//		decision =0;
		//}
		//
		//if ( var0==2 && var1==2) {
		//	if ( randFloat(0.0f, 1.0f) > 0.0f )
		//		decision =1;
		//}

		//if ( var0==1 && var1 ==2) {
		//	if ( randFloat(0.0f, 1.0f) > 0.5f )
		//	decision=1;
		//}
		//if ( var0==2 && var1 ==1) {
		//	if ( randFloat(0.0f, 1.0f) > 0.5f )
		//	decision=1;
		//}
		//if ( var0==0 && var1==0) {
		//	if ( randFloat(0.0f, 1.0f) > 0.9f )
		//		decision =1;
		//}
		//if ( var0==0 && var1==1) {
		//	if ( randFloat(0.0f, 1.0f) > 0.9f )
		//		decision =1;
		//}
		//if ( var0==1 && var1==0) {
		//	if ( randFloat(0.0f, 1.0f) > 0.9f )
		//		decision =1;
		//}
		//if ( var0==1 && var1==1) {
		//	if ( randFloat(0.0f, 1.0f) > 0.6f )
		//		decision =1;
		//}
		//if ( var0==0 && var1==2) {
		//	if ( randFloat(0.0f, 1.0f) > 0.6f )
		//		decision =1;
		//}
		//if ( var0==2 && var1==0) {
		//	if ( randFloat(0.0f, 1.0f) > 0.6f )
		//		decision =1;
		//}
